import React from 'react'
import styles from './Menu.module.scss'
import { ReactComponent as MenuBtn } from '../../assets/images/icons/tune.svg'
import PropTypes from 'prop-types'

class Menu extends React.Component {
   static propTypes = {
      items: PropTypes.arrayOf(
         PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string,
            titleActive: PropTypes.string,
            isToggle: PropTypes.bool,
            toggleActive: PropTypes.bool,
            onClick: PropTypes.func,
            isDivider: PropTypes.bool,
         })
      ).isRequired,
   }
   state = {
      isMenuOpen: false,
   }

   constructor(props) {
      super(props)
      document.addEventListener('click', this.handleClickOutside, false)
   }

   componentWillUnmount() {
      document.removeEventListener('click', this.handleClickOutside, false)
   }

   handleClickOutside = (e) => {
      const burgerMenu = document.getElementById('menuBtn')
      const path = e.path || (e.composedPath && e.composedPath())

      if (!path.includes(burgerMenu) && this.state.isMenuOpen) {
         this.setState({
            isMenuOpen: false,
         })
      }
   }

   toggleMenu = () => {
      this.setState((state) => ({ isMenuOpen: !state.isMenuOpen }))
   }

   renderItems = () => {
      return this.props.items.map((item) => {
         if (item.isDivider) {
            return <div key={item.id} className={styles.menu__divider} />
         }
         return (
            <button
               key={item.id}
               className={styles.menu__item}
               onClick={item.onClick}
            >
               {item.isToggle
                  ? item.toggleActive
                     ? item.titleActive
                     : item.title
                  : item.title}
            </button>
         )
      })
   }

   render() {
      const activeBtn = this.state.isMenuOpen ? styles.menu__mainBtn_active : ''
      return (
         <div className={styles.menu} id="menuBtn">
            <MenuBtn
               className={`${styles.menu__mainBtn} ${activeBtn}`}
               onClick={this.toggleMenu}
            />
            {this.state.isMenuOpen && (
               <div className={styles.menu__body}>{this.renderItems()}</div>
            )}
         </div>
      )
   }
}

export default Menu
