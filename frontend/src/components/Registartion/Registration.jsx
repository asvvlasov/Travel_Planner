import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './Registration.module.scss'
import { NavLink } from 'react-router-dom'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
   register,
   getInvitedEmail,
   registerByInvitation,
   login,
   emailConfirmation,
} from '../../redux/auth/operations'
import InputControl from '../../controls/Input/InputControl'
import Button from '../../controls/Button/Button'
import Switch from '../../controls/Switch/Switch'
import ResetForm from '../Forms/ResetForm'
import { ReactComponent as Logo } from '../../assets/images/icons/logo.svg'

class Registration extends Component {
   static propTypes = {
      register: PropTypes.func,
      login: PropTypes.func,
      regError: PropTypes.object,
      authError: PropTypes.object,
      invitedEmail: PropTypes.string,
      confirmedEmail: PropTypes.string,
   }

   state = {
      isModalOpen: false,
      email: '',
      password: '',
      rememberMe: false,
      emailErrorLabel: '',
      passwordErrorLabel: '',
      tabs: [
         {
            _id: 'signin',
            title: 'Вход',
            emailPlaceholder: 'Email',
            passwordPlaceholder: 'Пароль',
            inputStyle: 'input_signin',
            tabStyle: 'tabs_signin',
            footerStyle: 'footer_signin',
            btnText: 'Войти',
            btnOnClick: () => this.login(),
         },
         {
            _id: 'signup',
            title: 'Регистрация',
            emailLabel: 'Введите почту',
            emailLabelMobile: 'Почта',
            emailHintLabel: 'она же будет логином',
            passwordLabel: 'Придумайте пароль',
            passwordLabelMobile: 'Пароль',
            passwordHintLabel: 'не менее 6 символов',
            inputStyle: 'input_signup',
            tabStyle: 'tabs_signup',
            footerStyle: 'footer_signup',
            btnText: 'Зарегистрироваться',
            btnOnClick: () => this.register(),
         },
      ],
   }
   handleChange = (event) => {
      this.setState({
         [event.target.name]: event.target.value,
      })
   }
   passwordIsValid = (value) => {
      value.length > 5
         ? this.setState({ passwordErrorLabel: '' })
         : this.setState({ passwordErrorLabel: 'Пароль менее 6 символов' })
   }
   emailIsValid = (value) => {
      const reg = new RegExp(/^[\w._-]+@[\w._-]+(\.[a-z]{2,6})$/i)
      reg.test(value)
         ? this.setState({ emailErrorLabel: '' })
         : this.setState({ emailErrorLabel: 'Проверьте введеный email' })
   }

   register = async () => {
      const password = this.state.password
      const email = this.state.email.toLowerCase()

      const linkId = this.props.match.params.linkId
      if (linkId) {
         this.setState({ email: this.props.invitedEmail })
         await this.props.registerByInvitation({ linkId, password })
      } else {
         await this.props.register({ email, password })
      }

      if (!this.props.regError) {
         this.props.push('/home/signin')
      }
   }

   login = async () => {
      const { password, rememberMe } = this.state
      const email = this.state.email.toLowerCase()

      await this.props.login({ email, password, rememberMe })
      if (!this.props.authError) {
         this.props.push('/profile/travels')
      }
   }

   openModal = () => {
      this.setState({ isModalOpen: true })
   }

   closeModal = () => {
      this.setState({ isModalOpen: false })
   }

   mapTabsToRender = () =>
      this.state.tabs.map((tab) => (
         <NavLink
            to={tab._id === 'signin' ? '/home/signin' : '/home/signup'}
            className={styles.tabs__link}
            activeClassName={styles.tabs__link_active}
            children={tab.title}
            key={tab._id}
         />
      ))

   componentDidMount = () => {
      const { tabs } = this.state
      const tab = tabs.find((tab) => tab._id === this.props.match.params.tab)
      const linkId = this.props.match.params.linkId
      if (tab._id === 'signup' && linkId) {
         this.props.getInvitedEmail(linkId)
      } else if (tab._id === 'signin' && linkId) {
         this.props.emailConfirmation(linkId)
      }
   }
   componentDidUpdate = (prevProps) => {
      const { invitedEmail, confirmedEmail } = this.props
      if (
         prevProps.invitedEmail !== invitedEmail ||
         prevProps.confirmedEmail !== confirmedEmail
      ) {
         this.setState({ email: invitedEmail || confirmedEmail })
      }
   }

   render() {
      const {
         email,
         password,
         emailErrorLabel,
         passwordErrorLabel,
         tabs,
      } = this.state
      const { invitedEmail, confirmedEmail, match } = this.props

      const tab = tabs.find((tab) => tab._id === match.params.tab)

      return (
         <div className={styles.page}>
            <Logo className={styles.page__logo} />
            <div className={styles.formWrapper}>
               <div className={styles.form}>
                  <nav
                     className={`${styles.tabs} ${styles[tab.tabStyle]}`}
                     children={this.mapTabsToRender()}
                  />
                  {this.state.isModalOpen && (
                     <ResetForm onClose={this.closeModal} />
                  )}
                  <InputControl
                     type="text"
                     name="email"
                     styles={`${styles.input} ${styles.input_desktop} ${
                        styles[tab.inputStyle]
                     }`}
                     placeholder={tab.emailPlaceholder}
                     label={tab.emailLabel}
                     hintLabel={tab.emailHintLabel}
                     errorLabel={email && emailErrorLabel}
                     value={email}
                     disabled={!!invitedEmail || !!confirmedEmail}
                     onChange={this.handleChange}
                     onBlur={(e) => this.emailIsValid(e.target.value)}
                  />
                  <InputControl
                     type="password"
                     name="password"
                     styles={`${styles.input} ${styles.input_desktop} ${
                        styles[tab.inputStyle]
                     }`}
                     placeholder={tab.passwordPlaceholder}
                     label={tab.passwordLabel}
                     hintLabel={tab.passwordHintLabel}
                     errorLabel={password && passwordErrorLabel}
                     value={password}
                     onChange={this.handleChange}
                     onBlur={(e) => this.passwordIsValid(e.target.value)}
                  />
                  <InputControl
                     type="text"
                     name="email"
                     styles={`${styles.input} ${styles.input_mobile} ${
                        styles[tab.inputStyle]
                     }`}
                     placeholder={tab.emailPlaceholder}
                     label={tab.emailLabelMobile}
                     hintLabel={tab.emailHintLabel}
                     errorLabel={email && emailErrorLabel}
                     value={email}
                     disabled={!!invitedEmail || !!confirmedEmail}
                     onChange={this.handleChange}
                     onBlur={(e) => this.emailIsValid(e.target.value)}
                  />
                  <InputControl
                     type="password"
                     name="password"
                     styles={`${styles.input} ${styles.input_mobile} ${
                        styles[tab.inputStyle]
                     }`}
                     placeholder={tab.passwordPlaceholder}
                     label={tab.passwordLabelMobile}
                     hintLabel={tab.passwordHintLabel}
                     errorLabel={password && passwordErrorLabel}
                     value={password}
                     onChange={this.handleChange}
                     onBlur={(e) => this.passwordIsValid(e.target.value)}
                  />
                  {this.props.match.params.tab === 'signin' && (
                     <>
                        <div
                           className={`${styles.switch} ${styles.switch_desktop}`}
                        >
                           <Switch
                              labelText="запомнить меня"
                              checked={this.props.rememberMe}
                              onChange={(value) => {
                                 this.setState({
                                    rememberMe: value,
                                 })
                              }}
                           />
                        </div>
                        <div
                           className={`${styles.switch} ${styles.switch_mobile}`}
                        >
                           <Switch
                              labelText="запомнить меня"
                              checked={this.props.rememberMe}
                              type="mobile"
                              onChange={(value) => {
                                 this.setState({
                                    rememberMe: value,
                                 })
                              }}
                           />
                        </div>
                     </>
                  )}
                  <div
                     className={`${styles.footer} ${styles[tab.footerStyle]}`}
                  >
                     <Button
                        onClick={tab.btnOnClick}
                        text={tab.btnText}
                        disabled={
                           !email ||
                           !password ||
                           !!emailErrorLabel ||
                           !!passwordErrorLabel
                        }
                     />
                     {this.props.match.params.tab === 'signin' && (
                        <span
                           className={styles.footer__link}
                           onClick={this.openModal}
                        >
                           Забыли пароль?
                        </span>
                     )}
                  </div>
               </div>
            </div>
         </div>
      )
   }
}
const mapStateToProps = ({ fetchReducer, authReducer }) => ({
   regError: fetchReducer.registerAlert,
   authError: fetchReducer.loginError,
   invitedEmail: authReducer.invitedEmail,
   confirmedEmail: authReducer.confirmedEmail,
})

const mapDispatchToProps = (dispatch) =>
   bindActionCreators(
      {
         register,
         login,
         push,
         getInvitedEmail,
         registerByInvitation,
         emailConfirmation,
      },
      dispatch
   )

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
