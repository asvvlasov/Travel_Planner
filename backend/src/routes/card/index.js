const router = require('express').Router()
const CardModel = require('../../models/card.js')
const TravelModel = require('../../models/travel.js')
const asyncHandler = require('express-async-handler')
const fileRouter = require('./file')
const payerRouter = require('./payer')
const authenticateMiddleware = require('../../middlewares/authentication')

router.use(authenticateMiddleware)

router.use('/file', fileRouter)
router.use('/payer', payerRouter)

router
   .route('/')
   .post(
      asyncHandler(async (req, res) => {
         const { travelId } = req.body
         const newCard = await CardModel.create(req.body)
         await TravelModel.findByIdAndUpdate(travelId, { $push: { cards: newCard.id } })
         res.json({ data: newCard })
      })
   )
   .put(
      asyncHandler(async (req, res) => {
         const card = { ...req.body }
         delete card.users
         delete card.files
         delete card.payers
         res.json({ data: await CardModel.findByIdAndUpdate(card._id, card, { new: true }) })
      })
   )
router.route('/:cardId').delete(
   asyncHandler(async (req, res) => {
      const { cardId } = req.params
      let deletedCard = await CardModel.findByIdAndRemove(cardId)
      await TravelModel.findByIdAndUpdate(deletedCard.travelId, { $pull: { cards: cardId } })
      res.json({ data: deletedCard })
   })
)
router.route('/:cardType/:travelId').get(
   asyncHandler(async (req, res) => {
      const { cardType, travelId } = req.params
      res.json({ data: await CardModel.getCardsByCardType(cardType, travelId) })
   })
)

module.exports = router
