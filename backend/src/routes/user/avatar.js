const router = require('express').Router()
const UserModel = require('../../models/user.js')
const FileModel = require('../../models/file.js')
const fileMiddleware = require('../../middlewares/file.js')
const asyncHandler = require('express-async-handler')

router
   .route('/')
   .post(
      fileMiddleware,
      asyncHandler(async (req, res) => {
         let [file] = await FileModel.createFiles(req.files)
         const update = { avatar: file }
         const plainUser = JSON.parse(
            JSON.stringify(await UserModel.findByIdAndUpdate(req.user._id, update, { new: true }))
         )
         delete plainUser.password
         res.json({ data: plainUser })
      })
   )
   .delete(
      asyncHandler(async (req, res) => {
         const fileId = (await UserModel.findById(req.user._id)).avatar
         await FileModel.deleteFiles([fileId])
         const update = { avatar: null }
         const plainUser = JSON.parse(
            JSON.stringify(await UserModel.findByIdAndUpdate(req.user._id, update, { new: true }))
         )
         delete plainUser.password
         res.json({ data: plainUser })
      })
   )
router.route('/:fileId').get(
   asyncHandler(async (req, res) => {
      const { fileId } = req.params
      res.send((await FileModel.getFile(fileId)).Body)
   })
)

module.exports = router
