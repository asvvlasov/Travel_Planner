const router = require('express').Router()
const UserModel = require('../../models/user')
const asyncHandler = require('express-async-handler')
const contactRouter = require('./contact')
const avatarRouter = require('./avatar')

router.use('/contact', contactRouter)
router.use('/avatar', avatarRouter)

router
   .route('/')
   .get(
      asyncHandler(async (req, res) => {
         const user = JSON.parse(JSON.stringify(await UserModel.findById(req.user._id)))
         delete user.password
         res.json({ data: user })
      })
   )
   .put(
      asyncHandler(async (req, res) => {
         const user = { ...req.body }
         user._id = req.user._id
         delete user.contacts
         delete user.travels
         delete user.email
         const updatedUser = JSON.parse(
            JSON.stringify(await UserModel.findByIdAndUpdate(req.user._id, user, { new: true }))
         )
         delete updatedUser.password
         res.json({ data: updatedUser })
      })
   )

module.exports = router
