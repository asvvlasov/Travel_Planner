const express = require('express')
const router = express.Router()
const passport = require('../../middlewares/passport')
const cookieHandler = require('../../middlewares/cookieHandler')
const asyncHandler = require('express-async-handler')
const RegistrationModel = require('../../models/registration')
const UserModel = require('../../models/user')
const Errors = require('../../models/types/errors')

router.use(cookieHandler)
router.route('/:linkId').post(
   asyncHandler(async (req, res) => {
      const invite = await RegistrationModel.findById(req.params.linkId)
      if (!invite) {
         throw Errors.authError.notFoundError
      }
      const user = await UserModel.findById(invite.user)
      await RegistrationModel.findByIdAndDelete(invite.id)
      const plainUser = JSON.parse(JSON.stringify(user))
      delete plainUser.password
      req.user = plainUser
      res.json({
         data: req.user,
         ...Errors.success.confirmSuccess,
      })
   })
)
router.route('/').post(
   asyncHandler(passport.authenticate),
   asyncHandler(async (req, res) => {
      res.json({ data: req.user })
   })
)

module.exports = router
