const bunyan = require('bunyan')

const serializers = {
   req: (req) => {
      if (!req || !req.connection) return req
      return {
         method: req.method,
         url: req.url,
         remoteAddress: req.connection.remoteAddress,
         remotePort: req.connection.remotePort,
      }
   },
   res: (res) => {
      if (!res || !res.statusCode) return res
      return {
         statusCode: res.statusCode,
         header: res._header,
      }
   },
}

const logger = bunyan.createLogger({
   name: 'travel-keeper',
   serializers: {
      req: serializers.req,
      res: serializers.res,
      err: bunyan.stdSerializers.err,
   },
   streams: [
      {
         stream: process.stdout,
         level: 'fatal',
      },
      {
         stream: process.stdout,
         level: 'error',
      },
      {
         stream: process.stdout,
         level: 'info',
      },
   ],
})

logger.reqLog = (req) => {
   const log = logger.child(
      {
         id: req.id,
         body: req.body,
         method: req.method,
         url: req.url,
         remoteAddress: req.connection.remoteAddress,
         remotePort: req.connection.remotePort,
         headers: req.headers,
      },
      true
   )
   log.info('request')
}

logger.resLog = (res) => {
   const log = logger.child(
      {
         id: res.id,
         body: res.body,
         method: res.method,
         statusCode: res.statusCode,
      },
      true
   )
   log.info('response')
}

logger.middleware = (req, res, next) => {
   const afterResponse = () => {
      res.removeListener('finish', afterResponse)
      res.removeListener('close', afterResponse)
      logger.resLog(res)
   }
   res.on('finish', afterResponse)
   res.on('close', afterResponse)
   logger.reqLog(req)
   next()
}

module.exports = logger
