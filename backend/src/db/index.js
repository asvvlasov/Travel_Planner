const mongoose = require('mongoose')
const logger = require('../logger')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)

const connect = (app) => {
   const db = mongoose.connection
   const dbOptions = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
      auto_reconnect: true,
   }
   db.on('connecting', function () {
      logger.info('connecting to MongoDB...')
   })

   db.on('error', function (error) {
      logger.fatal({ err: error }, 'Error in MongoDb connection ')
      mongoose.disconnect()
   })
   db.on('connected', function () {
      logger.info('MongoDB connected!')
   })
   db.once('open', function () {
      logger.info('MongoDB connection opened!')
   })
   db.on('reconnected', function () {
      logger.info('MongoDB reconnected!')
   })
   db.on('disconnected', function () {
      logger.warn('MongoDB disconnected!')
   })
   mongoose.connect(process.env.DB_URL, dbOptions)
   app.use(
      session({
         resave: true,
         saveUninitialized: false,
         secret: process.env.SESSION_SECRET_PHRASE,
         store: new MongoStore({
            mongooseConnection: db,
         }),
      })
   )
}

module.exports = { connect }
