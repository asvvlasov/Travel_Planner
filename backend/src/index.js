const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const passport = require('./middlewares/passport')
const logger = require('./logger')
logger.info('server is starting')
if (!process.env.PRODUCTION) {
   const dotenv = require('dotenv')
   dotenv.config()
}

const app = express()
const PORT = process.env.PORT || 3300
const router = require(path.resolve(__dirname, '.', 'routes'))
const db = require('./db')
db.connect(app)

app.use(express.json())
   .use(express.urlencoded({ extended: true }))
   .use(cors())
   .use('/', express.static(path.resolve(__dirname, '..', 'opt', 'static')))
   .use(cookieParser(process.env.COOKIE_SECRET_KEY))
   .use(passport.initialize)
   .use(passport.session)
   .use(logger.middleware)
   .use(router)
   .listen(PORT, () => {
      logger.info(`server has been started at http://localhost:${PORT}`)
   })
